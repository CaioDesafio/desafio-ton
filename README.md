# desafio-ton

## Resumo técnico

Solução para o desafio proposto pela Stone, escrito em TypeScript. Conforme a especificação do desafio, este projeto implementa uma API REST com 4 rotas, sendo elas usadas para:

* Incrementar ou consultar o número de acessos;
* Adicionar novos usuários ou consultar usuários existentes.

Arquivos de código-fonte encontram-se na pasta `src`. O 'ponto de entrada' da aplicação é o arquivo `index.ts`. Todos os arquivos estão em TypeScript puro. Sempre que possível, objetos JSON foram descritos usando interfaces. As implementações de cada endpoint encontram-se na pasta `routes`, e os arquivos relacionados ao banco de dados e persistência ficam na pasta `orm`.

A aplicação foi desenvolvida usando a metodologia BDD. Os testes podem ser encontrados na pasta `tests`. Eles são nomeados de acordo com qual parte do software testam, e em qual ordem aquela suíte de testes é executada. Por exemplo, o arquivo `00 - model.spec.ts` descreve os testes de banco (chamado também de *model*), e é o primeiro a ser executado.

O banco de dados escolhido foi o SQLite, visando simplificar a infra caso alguém queira testar localmente. Porém, a biblioteca TypeORM, usada neste projeto, suporta múltiplos backends, e seria possível trocar o banco por MySQL ou MongoDB apenas editando o arquivo `src/orm/model.ts`.

As contas de usuário não são armanezadas com a senha em texto puro. A API espera que o cliente mande uma hash da senha (SHA-256), e a aplicação gera uma *salt* na criação do usuário. Então, salvamos ambas a salt e uma hash dos dois valores. Dessa forma, o servidor não precisa guardar nem a senha, nem a hash direa dela. 

Salvo por erro humano, todas as variáveis são explicitamente tipadas, similar a linguagens como Java.

## Rodando a aplicação

```
npm install
npm start
```

Rodar o `npm start` irá executar as suítes de testes antes de iniciar o servidor. Se quiser apenas testar, basta executar `npm run build` após ter instalado as dependências.