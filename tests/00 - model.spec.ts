import { expect } from "chai";
import * as crypto from "crypto";
import { Hash } from "crypto";
import "mocha";

import model from '../src/orm/model';
import { ERole, User } from '../src/orm/user';

describe("The database", async function() {

    describe("should...", async function() {

        it("Initialize without problems.", async function() {

            await model.initialize();

            expect(model.isInitialized).to.be.true;

        });

        it("Be able to insert new items.", async function() {

            var user:User = new User();
            user.name = "Caio";
            user.role = ERole.ADMIN;
            user.saltPassword(crypto.createHash("sha256").update("super secret password").digest('hex'));
            await model.manager.save(user);
            expect(user.id).to.not.be.null;

        });

        it("Have values in it after an insert.", async function() {

            var users = await model.manager.find(User);
            expect(users.length).to.not.equal(0);

        })

    })

});