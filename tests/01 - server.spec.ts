import { expect } from "chai";
import * as crypto from "crypto";
import "mocha";
import RestServer from "../src/server";
import { EResult, IApiResponse } from '../src/IApiResponse';
import { ERole } from "../src/orm/user";
import { IConnectionDetails } from "../src/IConnectionDetails";

/*

Note: "AxiosResponse" is a type that comes from the axios web request library.
"IApiResponse" is a project-specific interface that describes our API responses,
and it's what we expect to get after parsing the returned JSON, stored inside
'AxiosResponse.data'.

So we have the Express' Response type, the AxiosResponse type, and our own
IApiResponse interface, which are all different things.

*/
import axios, { AxiosResponse } from 'axios';
import { stringify } from "querystring";


describe("The RESTful server", async function() {

    describe("should...", async function() {

        this.timeout(5000);

        var serverDetails!:IConnectionDetails; // the '!' assures the TypeScript compiler this won't be null by the end of this function.
        var fullAddress:string;
        var anId:number;

        it("Start up without any issues.", async function() {
            
            serverDetails = await RestServer.start();
            fullAddress = serverDetails.address + ":" + serverDetails.port + "/users_api";

        })


        it("Return a 'Hello, world!' after a GET request on '/'", async function() {
          
            var httpResponse:AxiosResponse = await axios.get(fullAddress.replace("/users_api", ""));
            expect(httpResponse.data).to.equal("Hello, world!");        
            
        });

        it("Add a new user after a valid POST request to the '/users_api/add' endpoint.", async function() {
            
            var hash = crypto.createHash("sha256").update("some password").digest('hex');
            var newUserData = { name: "Jotaro", role: ERole.USER, hash: hash };
            var response:AxiosResponse = await axios.post(fullAddress + '/add', newUserData);
            var apiResponse:IApiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.SUCCESS);
            expect(apiResponse.payload).to.be.an("object");
            expect(apiResponse.payload.id).to.not.be.null;
            expect(apiResponse.payload.name).to.equals(newUserData.name);
            expect(apiResponse.payload.role).to.equals(ERole.USER);

            anId = apiResponse.payload.id;
            
        });

        it("Allow you to find an user by its id, with a GET request to the '/users_api/:id' endpoint.", async function() {
            
            var response:AxiosResponse = await axios.get(fullAddress + '/' + anId.toString());
            var apiResponse:IApiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.SUCCESS);
            expect(apiResponse.payload).to.be.an("object");
            expect(apiResponse.payload.id).to.not.be.null;
            expect(apiResponse.payload.name).to.equals("Jotaro");
            expect(apiResponse.payload.role).to.equals(ERole.USER);
            
        });

        it("Return an error if you look for an user that does not exist.", async function() {
            
            var response:AxiosResponse = await axios.get(fullAddress + '/' + String(999999));
            var apiResponse:IApiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.ERROR);
            expect(apiResponse.payload).to.equal("user not found");
            
        });

        it("NOT add a new user if the POST data is invalid or incomplete.", async function() {
            
            var newUserData = {};
            var response:AxiosResponse = await axios.post(fullAddress + '/add', newUserData);
            var apiResponse:IApiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.ERROR);
            expect(apiResponse.payload).to.equal("missing fields");

            var hash = crypto.createHash("sha256").update("another password").digest('hex');
            newUserData = { name: "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", role:ERole.USER, hash: hash };
            response = await axios.post(fullAddress + '/add', newUserData);
            apiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.ERROR);
            expect(apiResponse.payload).to.equal("name too long");

            newUserData = { name: "Random Dude", role:26, hash: hash };
            response = await axios.post(fullAddress + '/add', newUserData);
            apiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.ERROR);
            expect(apiResponse.payload).to.equal("unknown role");

            newUserData = { name: "John Smith", role:ERole.USER, hash: "not hashed" };
            response = await axios.post(fullAddress + '/add', newUserData);
            apiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.ERROR);
            expect(apiResponse.payload).to.equal("invalid password");
            
        });

        it("Return an array of users after a GET request to the '/users_api/getAll' endpoint.", async function() {
            
            var response:AxiosResponse = await axios.get(fullAddress + '/getAll');
            var apiResponse:IApiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.SUCCESS);

            expect(apiResponse.payload).to.be.an("array");
            expect(apiResponse.payload.length).to.be.greaterThan(0);
            
            expect(apiResponse.payload[0]).to.haveOwnProperty("name");
            expect(apiResponse.payload[0]).to.not.haveOwnProperty("saltedHash");
            expect(apiResponse.payload[0]).to.not.haveOwnProperty("salt");
            expect(apiResponse.payload[0].name).to.not.be.null;
            expect(apiResponse.payload[0].role).to.not.be.null;
            
        });

    });
  
});