import { expect } from "chai";
import "mocha";
import RestServer from "../src/server";
import { EResult, IApiResponse } from '../src/IApiResponse';
import model from '../src/orm/model';
import axios, { AxiosResponse } from 'axios';
import { IConnectionDetails } from "../src/IConnectionDetails";

describe("The Access Count API", async function() {

    describe("should...", async function() {

        this.timeout(5000);

        var serverDetails!:IConnectionDetails;
        var fullAddress:string;
        
        it("Return the access count after a GET request to the '/count_api/getAccessCount' endpoint", async function() {
            
            serverDetails = RestServer.getInfo();
            fullAddress = serverDetails.address + ":" + serverDetails.port;
             
            var response:AxiosResponse = await axios.get(fullAddress + '/count_api/getAccessCount');
            var apiResponse:IApiResponse = response.data;

            expect(apiResponse.result).to.equal(EResult.SUCCESS);
            expect(apiResponse.payload).to.be.a("number");
            expect(apiResponse.payload).to.not.be.NaN; // "NaN" is a "number" as far as JS types go ¯\_(ツ)_/¯
            expect(apiResponse.payload).to.be.greaterThan(-1);
            
        });

        it("Increase the access count by 1 after a GET request to the '/count_api/incrementAccessCount' endpoint", async function() {
            
            var httpResponse:AxiosResponse;
            var apiResponse:IApiResponse;

            var oldAccessCount:number;
            var newAccessCount:number;

            httpResponse = await axios.get(fullAddress + '/count_api/getAccessCount');
            apiResponse = httpResponse.data;
            oldAccessCount = apiResponse.payload;
            
            httpResponse = await axios.get(fullAddress + '/count_api/incrementAccessCount');
            apiResponse = httpResponse.data;
            newAccessCount = apiResponse.payload;

            expect(newAccessCount).to.be.greaterThan(oldAccessCount); 
            
        });

        it("Close the server after testing is over and destroy the test database.", async function () {

            serverDetails.httpServerObject.close();
            expect(serverDetails.httpServerObject.listening).to.be.false;
            await model.dropDatabase();
            await model.destroy();

        });

    });
  
});