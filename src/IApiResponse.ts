export interface IApiResponse {

    result:EResult;

    payload?:any;

}

export enum EResult {

    SUCCESS = "Success",
    
    ERROR = "Error"

}