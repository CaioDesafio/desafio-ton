import { Server } from "http";


export interface IConnectionDetails {

    address: string;

    port: number;
    
    httpServerObject: Server;

}
