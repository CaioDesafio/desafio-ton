import "reflect-metadata";

import express from "express";
import { Request, Response, Express } from "express";

import CountApiRouter from "./routes/countApiRouter";
import UsersApiRouter from "./routes/usersApiRouter";
import { IConnectionDetails } from "./IConnectionDetails";

export default class RestServer {

    private static info:IConnectionDetails;
    private static port:number = 8081;

    static async start():Promise<IConnectionDetails> {

        const expressServer:Express = express();
        expressServer.use(express.json());
        expressServer.use(express.urlencoded({extended: true}));

        expressServer.use('/count_api', CountApiRouter);
        expressServer.use('/users_api', UsersApiRouter);
        

        expressServer.get('/', function(request:Request, response:Response) {

            response.send("Hello, world!");

        });

        RestServer.info = { 
            address: 'http://localhost', 
            port: RestServer.port, 
            httpServerObject: await expressServer.listen(RestServer.port)
        };
        
        return RestServer.info;

    }

    static getInfo():IConnectionDetails {

        return RestServer.info;

    }

}