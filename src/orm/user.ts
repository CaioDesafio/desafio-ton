import { Entity, Column, PrimaryGeneratedColumn } from "typeorm"
import * as crypto from "crypto";

@Entity({ name: "users" })
export class User {

    @PrimaryGeneratedColumn() id:number;

    @Column() name:string;

    @Column({ select: false }) salt:string;

    @Column({ select: false }) saltedHash:string;
    
    @Column() role:ERole;

    saltPassword(simpleHash:string):void {

        this.salt = crypto.randomBytes(64).toString();;
        this.saltedHash = crypto.createHash("sha256").update(simpleHash + this.salt).digest('hex');

    }

}

export enum ERole {
    USER = "User",
    ADMIN = "Admin"
}