import "reflect-metadata";
import { DataSource } from "typeorm";
import { User } from "./user";

const model:DataSource = new DataSource({
    type: "better-sqlite3",
    database: "database.sqlite",
    synchronize: true,
    logging: false,
    entities: [User],
    migrations: [],
    subscribers: [],
});

export default model;