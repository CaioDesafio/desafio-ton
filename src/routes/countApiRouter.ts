import express, { Request, Response, Express } from "express";
import axios, { AxiosResponse } from 'axios';
import { EResult, IApiResponse } from '../IApiResponse';

const countApiNamespace:string = "desafio-ton";
const countApiKey:string = "05255c01-39c8-4106-95f1-5a97b7ef152c";

const CountApiRouter = express.Router();

CountApiRouter.get('/incrementAccessCount', async function(request:Request, response:Response) {

    var apiResponse:IApiResponse;

    axios.get(`https://api.countapi.xyz/hit/${countApiNamespace}/${countApiKey}`).then( function(httpResponse:AxiosResponse) {

        apiResponse = { result: EResult.SUCCESS, payload: httpResponse.data.value };
        response.json(apiResponse);

    }).catch(function(error:any) {

        response.json({ result: EResult.ERROR });

    });

});

CountApiRouter.get('/getAccessCount', async function(request:Request, response:Response) {

    var apiResponse:IApiResponse;

    axios.get(`https://api.countapi.xyz/get/${countApiNamespace}/${countApiKey}`).then( function(httpResponse:AxiosResponse) {

        apiResponse = { result: EResult.SUCCESS, payload: httpResponse.data.value };
        response.json(apiResponse);

    }).catch(function(error:any) {

        response.json({ result: EResult.ERROR });
        
    });

});

export default CountApiRouter;