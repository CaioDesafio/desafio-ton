import express, { Request, Response } from "express";
import { Repository } from "typeorm";
import { EResult, IApiResponse } from '../IApiResponse';
import model from '../orm/model';
import { ERole, User } from "../orm/user";

const UsersApiRouter = express.Router();
const UsersRepository:Repository<User> = model.getRepository(User);

UsersApiRouter.post('/add', async function(request:Request, response:Response) {

    var newUserData:{ name: string; role:ERole, hash:string } = request.body;

    if (!newUserData.name || !newUserData.role || !newUserData.hash) {
        response.json({ result: EResult.ERROR, payload: "missing fields" });
        return;
    }

    if (newUserData.name.length > 32) {
        response.json({ result: EResult.ERROR, payload: "name too long" });
        return;          
    }

    if (newUserData.hash.length != 64) {
        response.json({ result: EResult.ERROR, payload: "invalid password" });
        return;          
    }

    if (newUserData.role != ERole.ADMIN && newUserData.role != ERole.USER) {
        response.json({ result: EResult.ERROR, payload: "unknown role" });
        return;             
    }

    var user:User = new User();
    user.name = newUserData.name;
    user.role = newUserData.role;
    user.saltPassword(newUserData.hash);
    await model.manager.save(user);

    response.json({
        result: EResult.SUCCESS,
        payload: {
            name: user.name,
            role: user.role,
            id: user.id
        }
    });

});

UsersApiRouter.get('/getAll', async function(request:Request, response:Response) {

    var users:Array<User> = await UsersRepository.find();
    response.json({ 
        result: EResult.SUCCESS, 
        payload: users
    });

});

UsersApiRouter.get('/:id', async function(request:Request, response:Response) {

    var user:User|null = await UsersRepository.findOneBy({ id: Number(request.params.id) });
    if (user) {
        
        response.json({ 
            result: EResult.SUCCESS, 
            payload: user
        });
        return;

    }

    response.json({ 
        result: EResult.ERROR, 
        payload: "user not found"
    });    

});

export default UsersApiRouter;